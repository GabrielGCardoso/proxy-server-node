const express = require('express');
const axios = require('axios');
const cors = require('cors');
const app = express();
const port = 5555; // Porta do servidor proxy

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));


app.all('/*', async (req, res) => {
  let targetUrl = 'https://tests.plataformatarget.com.br'; // Substitua pelo URL do servidor de destino desejado
  // let targetUrl = 'https://drogaraia.com.br/api/next/middlewareGraphql'; // Substitua pelo URL do servidor de destino desejado


  // let targetUrl = 'http://localhost:3002'
  // let testEndpoint = 'https://bff.stage2.drogaraia.com.br'; // Substitua pelo URL do servidor de destino desejado
  // const testEndpoint = 'http://localhost:3002/graphql'
  const testEndpoint = 'https://core.plataformatarget.com.br'
  // const testEndpoint = 'https://drogaraia.com.br/api/next/middlewareGraphql'
  // const testGraphqlCommand = 'placeOrder'
  const testGraphqlCommand = 'placeOrder'

  const { method, url, headers, body } = req;

  try {
    let toRedirect = false;

    
    delete headers.host; // evita bloquear proxy request cloud front
    delete headers.accept; // evita bloquear proxy request cloud front

    let requestOptions = {
      method: method,
      headers: { ...headers },
    };

    const isPostRequest = method === 'POST';
    // let req = null;
    if (isPostRequest) {
      requestOptions.data = body;

      // mandar request de test para servidor local
      const stringifiedBody = JSON.stringify(body);
      toRedirect = stringifiedBody.includes(testGraphqlCommand)

      //if(toRedirect){
      //  targetUrl = testEndpoint;
      //  // targetUrl = testEndpoint + url;
//
      //  console.log("req-redirected", JSON.stringify({ url: targetUrl, ...requestOptions }));
      //}
    }

    const objReq = {
      // url: targetUrl,
      url: targetUrl + url,
      ...requestOptions,
    }

    const targetResponse = await axios.request(objReq);
    // if(toRedirect){
    //   console.log("response", JSON.stringify(targetResponse.data));
    // }

    console.log("req-obj", JSON.stringify({req: { url: targetUrl + url, ...requestOptions }, resp: targetResponse.data }));
    // Retorna a resposta do servidor de destino como resposta para a requisição original
    res.status(targetResponse.status).json(targetResponse.data);

  } catch (error) {
    console.error('Erro ao fazer a requisição para o servidor de destino:', error);
    res.status(500).send(error.message);
  }
});

app.listen(port, () => {
  console.log(`Servidor proxy rodando na porta ${port}`);
});
